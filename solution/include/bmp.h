#pragma once

#include <stdint.h>
#include <stdio.h>

#include "image.h"

#define BMP_MAGIC_BYTES1 0x4d42 /*MB*/
#define BMP_MAGIC_BYTES2 0x424d /*BM*/

#define HEADER_SIZE 54
#define BMP_HEADER_SIZE 40
#define BMP_BI_PLANES 1
#define BMP_BIT_COUNT 24
#define BMP_BI_COMPRESSION 0
#define MY_NAME_IN_ASCII 0x6B72614D
#define MY_GROUP_NUM_IN_ASCII 0x32333233

typedef struct __attribute__((packed)) bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bfOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
} bmp_header_t;

enum read_write_status {
    READ_UNKNOWN_ERROR,
    READ_OK,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    READ_END_OF_FILE, // feof
    READ_STREAM_ERROR, // ferror
    WRITE_UNKNOWN_ERROR,
    WRITE_OK,
    WRITE_ERROR,
};

enum read_write_status from_bmp(FILE* in, image_t* img);

enum read_write_status to_bmp(FILE* out, image_t* img);
