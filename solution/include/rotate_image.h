#pragma once

#include <stdint.h>
#include <stdio.h>

#include "image.h"

image_t* rotate(image_t* const img, int angle);
