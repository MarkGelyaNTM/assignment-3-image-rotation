#pragma once

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

typedef struct pixel {
    uint8_t b, g, r;
} pixel_t;

typedef struct image {
    uint64_t width, height;
    struct pixel* data;
} image_t;

image_t* new_image(uint64_t width, uint64_t height);

void delete_image(image_t* image);

void update_image(uint64_t width, uint64_t height, image_t* img);

struct pixel* get_pixel(image_t* img, uint64_t x, uint64_t y);

void set_pixel(image_t* img, uint64_t x, uint64_t y, pixel_t* pix);


/* DEBUG */
void print_image(const image_t* img);
