#pragma once

#include <stdint.h>
#include <stdio.h>

#include "bmp.h"

enum error_list {
    ERROR_INCORRECT_PARAMS,
    ERROR_INVALID_ANGLE,
    ERROR_FILE_CANT_BE_OPENED,
};

void errprt(enum error_list error);

void errprtIO(enum read_write_status error);
