#include "bmp.h"
#include "errprt.h"

void _errprt(char* msg) {
    fprintf(stderr, "%s\n", msg);
}

char* errors_text[] = {
    "Incorrect number of arguments. Expected: \"<app_name> <input_filename> <output_filename> <angle|incorrect=0>\"",
    "Invalid angle\n",
    "Error: Input file cannot be opened",
};

void errprt(enum error_list error) {
    _errprt(errors_text[error]);
}

void errprtIO(enum read_write_status error) {
    char *msg = "";
    switch (error) {
        case READ_END_OF_FILE: msg = "error READ_END_OF_FILE"; break;
        case READ_INVALID_HEADER: msg = "error READ_INVALID_HEADER"; break;
        case READ_STREAM_ERROR: msg = "error READ_STREAM_ERROR"; break;
        case READ_INVALID_BITS: msg = "error READ_INVALID_BITS"; break;
        case READ_INVALID_SIGNATURE: msg = "error READ_INVALID_SIGNATURE"; break;
        case READ_UNKNOWN_ERROR: msg = "error READ_UNKNOWN_ERROR"; break;
        case WRITE_ERROR: msg = "error WRITE_ERROR"; break;
        case WRITE_UNKNOWN_ERROR: msg = "error WRITE_UNKNOWN_ERROR"; break;
        default: break;
    }
    _errprt(msg);
}
