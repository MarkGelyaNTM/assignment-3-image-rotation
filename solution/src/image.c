#include "image.h"

image_t* new_image(uint64_t width, uint64_t height) {
    pixel_t* pixels = malloc(width * height * sizeof(pixel_t));
    image_t* img = malloc(sizeof(image_t));
    img->width = width;
    img->height = height;
    img->data = pixels;
    return img;
}

void update_image(uint64_t width, uint64_t height, image_t* img) {
    img->width = width;
    img->height = height;
    pixel_t* pixels = malloc(width * height * sizeof(pixel_t));
    free(img->data);
    img->data = pixels;
}

void delete_image(image_t* image) {
    if (image != NULL) {
        if (image->data != NULL)
            free(image->data);
        free(image);
    }
}

pixel_t* get_pixel(image_t* const img, uint64_t x, uint64_t y){
    return img->data + (y * img->width + x);
}

void set_pixel(image_t* img, uint64_t x, uint64_t y, pixel_t* pixel){
    pixel_t* ptr = img->data + (y * img->width + x);
    ptr->b = pixel->b;
    ptr->g = pixel->g;
    ptr->r = pixel->r;
}


/* DEBUG */
void print_image(const image_t* img) {
    if (img != NULL) {
        printf("Image{width: %lu, height: %lu, data: ", img->width, img->height);
        if (img->data != NULL) {
            printf("Pixel*[");
            for (uint64_t i = 0; i < img->width * img->height; i++) {
                if(i % img->width == 0) {
                    printf("\n");
                }
                printf("Pixel{b: %3u, g: %3u, r: %3u}", img->data[i].b, img->data[i].g, img->data[i].r);
                if (i < img->width * img->height - 1) {
                    printf(", ");
                }
            }
            printf("\n]");
        } else {
            printf("NULL");
        }
        printf("}\n");
    } else {
        printf("Image{NULL}");
    }
}

