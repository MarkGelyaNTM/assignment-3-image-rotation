#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include "bmp.h"
#include "image.h"

bmp_header_t* new_bmp_header(size_t width, size_t height){
    bmp_header_t* header = malloc(sizeof(bmp_header_t));
    header->bfType = BMP_MAGIC_BYTES1;
    header->bfileSize = sizeof(bmp_header_t) + 3 * width * height + (width * sizeof(pixel_t)) % 4 * height;
    header->bfReserved = 0;
    header->bfOffBits = sizeof(bmp_header_t);
    header->biSize = sizeof(bmp_header_t) - sizeof(uint32_t) * 3 - sizeof(uint16_t);
    header->biWidth = width;
    header->biHeight = height;
    header->biPlanes = BMP_BI_PLANES;
    header->biBitCount = BMP_BIT_COUNT;
    header->biCompression = BMP_BI_COMPRESSION;
    header->biSizeImage = header->bfileSize - sizeof(bmp_header_t);
    header->biXPelsPerMeter = MY_NAME_IN_ASCII;
    header->biYPelsPerMeter = MY_GROUP_NUM_IN_ASCII;
    header->biClrUsed = 0;
    header->biClrImportant = 0;
    return header;
}

void delete_bmp_header(bmp_header_t* header){
    free(header);
}

enum read_write_status from_bmp(FILE* in, image_t* img) {
    bmp_header_t header = {0};

    // Read first header (BITMAPFILEHEADER) and check magic bytes
    if (fread(&header, sizeof(bmp_header_t), 1, in) > 0)
        if ((header.bfType != BMP_MAGIC_BYTES1) && (header.bfType != BMP_MAGIC_BYTES2))
            return READ_INVALID_SIGNATURE;

    // Skip secont header (BITMAPFILEHEADER [CORE|3|4|5])
    if (fseek(in, (long) header.bfOffBits, SEEK_SET) != 0)
        return READ_INVALID_HEADER;

    update_image(header.biWidth, header.biHeight, img);

    const uint64_t line_size = header.biWidth * sizeof(struct pixel);
    const int padding = (int)((uint64_t)4 - line_size % 4);
    pixel_t* pointer = img->data;

    if (padding == 0) {
        // Read all bytes
        if (fread(pointer, header.biWidth * header.biHeight, 1, in) != header.biWidth * header.biHeight) {
            if (feof(in))
                return READ_END_OF_FILE;
            if (ferror(in))
                return READ_STREAM_ERROR;
        }
    } else {
        for(size_t i = 0; i < header.biHeight; i++) {
            fread(pointer, line_size, 1, in);
            pointer += header.biWidth;
            fseek(in, padding, SEEK_CUR);
        }
    }
    return READ_OK;
}

enum read_write_status to_bmp(FILE* out, image_t* img) {
    bmp_header_t* header = new_bmp_header(img->width, img->height);
    if(fwrite(header, sizeof(bmp_header_t), 1, out) != 1) {
        delete_bmp_header(header);
        return WRITE_ERROR;
    }

    const uint64_t line_size = header->biWidth * sizeof(pixel_t);
    const int padding = (int)(4 - line_size % 4);

    pixel_t* pointer = img->data;
    int zero = 0;
    if (padding == 0) {
        // Write all bytes
        if (fwrite(pointer, header->biWidth * header->biHeight, 1, out) != header->biWidth * header->biHeight) {
            delete_bmp_header(header);
            return WRITE_ERROR;
        }
    } else {
        for(size_t i = 0; i < header->biHeight; i++) {
            if (fwrite(pointer, line_size, 1, out) != 1) {
                delete_bmp_header(header);
                return WRITE_ERROR;
            }
            pointer += header->biWidth;
            if (fwrite(&zero, 1, padding, out) != padding) {
                delete_bmp_header(header);
                return WRITE_ERROR;
            }
        }
    }
    delete_bmp_header(header);
    return WRITE_OK;
}
