#include "rotate_image.h"
#include "image.h"

image_t* rotate(image_t* img, int angle){
    image_t* rotated_img;

    const uint64_t width = img->width;
    const uint64_t height = img->height;

    if(angle % 180 == 0)
        rotated_img = new_image(width, height);
    else
        rotated_img = new_image(height, width);

    switch(angle){
        case 0:
            for(uint64_t y = 0; y < height; y++)
                for(uint64_t x = 0; x < width; x++)
                    set_pixel(rotated_img, x, y, get_pixel(img, x, y));
            break;
        case 90:
            for(uint64_t y = 0; y < height; y++)
                for(uint64_t x = 0; x < width; x++)
                    set_pixel(rotated_img, y, width - x - 1, get_pixel(img, x, y));
            break;
        case 180:
            for(uint64_t y = 0; y < height; y++)
                for(uint64_t x = 0; x < width; x++)
                    set_pixel(rotated_img, width - x - 1, height - y - 1, get_pixel(img, x, y));
            break;
        case 270:
            for(uint64_t y = 0; y < height; y++)
                for(uint64_t x = 0; x < width; x++)
                    set_pixel(rotated_img, height - y - 1, x, get_pixel(img, x, y));
            break;
    }
    return rotated_img;
}
