#include <stdio.h>
#include <stdlib.h>

#include "bmp.h"
#include "errprt.h"
#include "image.h"
#include "rotate_image.h"

int main(int argc, char** argv) {
    // Check args
    if(argc != 4){
        errprt(ERROR_INCORRECT_PARAMS);
        return 1;
    }
    const char* input_filename = argv[1];
    const char* output_filename = argv[2];
    int angle = (360 + atoi(argv[3]) % 360) % 360;
    if(angle % 90 != 0){
        errprt(ERROR_INVALID_ANGLE);
        return 1;
    }

    // Read
    FILE* input_file = fopen(input_filename, "rb");
    if (input_file == NULL) {
        errprt(ERROR_FILE_CANT_BE_OPENED);
        return 1;
    }
    image_t* img = new_image(0, 0);
    enum read_write_status rs = from_bmp(input_file, img);
    fclose(input_file);
    // Check read
    if (rs != READ_OK) {
        errprtIO(rs);
        delete_image(img);
        return 1;
    }

    image_t* res = rotate(img, (int)angle);
    delete_image(img);

    // Write
    FILE* output_file = fopen(output_filename, "wb");
    if (output_file == NULL) {
        errprt(ERROR_FILE_CANT_BE_OPENED);
        delete_image(res);
        return 1;
    }
    enum read_write_status ws = to_bmp(output_file, res);
    fclose(output_file);
    // Write check
    if(ws != WRITE_OK){
        errprtIO(ws);
        delete_image(res);
        return 1;
    }

    delete_image(res);

    return 0;
}

